'use strict';

//console.log('Service Work iniciado.');

self.addEventListener('install', function (event) {
    self.skipWaiting();
    //console.log('Service Work instalado com sucesso.');
});

self.addEventListener('activate', function (event) {
    //console.log('Service Work ativado com sucesso.');
});

self.addEventListener('push', function (event) {
    var title = 'Titulo';
    var body = 'Voce tem novidades no !';
    var icon = 'images/icon.png';
    var url = '/';

    if (event.data) {
        var payload = JSON.parse(event.data.text());
        //console.log(payload);

        title = payload.title;
        body = payload.body;
        icon = payload.icon;
        url = payload.url;
    }

    event.waitUntil(
      self.registration.showNotification(
        title,
        {
            body: body,
            icon: icon,
            tag: url
        }
      ));
});

self.addEventListener('notificationclick', function (event) {
    // Android doesn't close the notification when you click on it  
    // See: http://crbug.com/463146  
    event.notification.close();

    // This looks to see if the current is already open and  
    // focuses if it is  
    event.waitUntil(
      clients.matchAll({
          type: "window"
      })
      .then(function (clientList) {
          for (var i = 0; i < clientList.length; i++) {
              var client = clientList[i];
              if (client.url == event.notification.tag && 'focus' in client)
                  return client.focus();
          }
          if (clients.openWindow) {
              return clients.openWindow(event.notification.tag);
          }
      })
    );
});