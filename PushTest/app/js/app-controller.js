class AppController {
    constructor() {
        // Define a different server URL here if desire.
        this._currentSubscription = { "endpoint": "", "keys": { "p256dh": "", "auth": "" } };

        this._PUSH_SERVER_URL = 'https://android.googleapis.com/gcm/send';
        this._API_KEY = '<YOUR_API_KEY>';

        this._applicationKeys = {
            publicKey: window.base64UrlToUint8Array(
              'BDd3_hVL9fZi9Ybo2UUzA284WG5FZR30_95YeZJsiApwXKpNcF1rRPF3foIiBHXRdJI2Qhumhf6_LFTeZaNndIo'),
            privateKey: window.base64UrlToUint8Array(
              'xKZKYRNdFFn8iQIF2MH54KTfUHwH105zBdzMR7SI3xI')
        };

        this.updatePushInfo();
    }

    updatePushInfo() {
        this._currentSubscription.endpoint = this.getQueryString('e');
        this._currentSubscription.keys.p256dh = this.getQueryString('p');;
        this._currentSubscription.keys.auth = this.getQueryString('a');

        const payloadText = "{\"body\":\"" + this.getQueryString('b') + "\", \"title\":\"" + this.getQueryString('t') + "\", \"icon\":\"" + this.getQueryString('i') + "\", \"url\":\"" + this.getQueryString('u') + "\"}";
        //console.log(payloadText);

        let payloadPromise = Promise.resolve(null);

        if (payloadText && payloadText.trim().length > 0) {
            payloadPromise = EncryptionHelperFactory.generateHelper()
            .then(encryptionHelper => {
                return encryptionHelper.encryptMessage(
                  JSON.parse(JSON.stringify(this._currentSubscription)), payloadText);
            });
        }

        const vapidPromise = Promise.resolve(null);

        return Promise.all([
          payloadPromise,
          vapidPromise
        ])
        .then(results => {
            const payload = results[0];
            const vapidHeaders = results[1];

            let infoFunction = this.getGCMInfo;
            if (this._currentSubscription.endpoint.indexOf(
              'https://android.googleapis.com/gcm/send') === 0) {
                infoFunction = () => {
                    return this.getGCMInfo(this._currentSubscription, payload,
                      this._API_KEY);
                };
            }

            const requestInfo = infoFunction();
            //console.log(JSON.stringify(this._currentSubscription));

            fetch('/api/SendPush/', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(requestInfo)
            }).then(function (response) {
                //console.log(response);
                return response;
            })
        });
    }

    getGCMInfo(subscription, payload, apiKey) {
        const headers = {};

        headers.Authorization = `key=${apiKey}`;
        headers['ContentType'] = `application/json`;

        const endpointSections = subscription.endpoint.split('/');
        const subscriptionId = endpointSections[endpointSections.length - 1];
        const gcmAPIData = {
            to: subscriptionId
        };

        if (payload) {
            gcmAPIData['raw_data'] = this.toBase64(payload.cipherText);
            headers.Encryption = `salt=${payload.salt}`;
            headers['CryptoKey'] = `dh=${payload.publicServerKey}`;
            headers['ContentEncoding'] = `aesgcm`;
        }

        return {
            headers: headers,
            body: JSON.stringify(gcmAPIData),
            endpoint: 'https://android.googleapis.com/gcm/send'
        };
    }

    toBase64(arrayBuffer, start, end) {
        start = start || 0;
        end = end || arrayBuffer.byteLength;

        const partialBuffer = new Uint8Array(arrayBuffer.slice(start, end));
        return btoa(String.fromCharCode.apply(null, partialBuffer));
    }

    getQueryString(field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
}

if (window) {
    window.AppController = AppController;
}
