'use strict';

if ('serviceWorker' in navigator) {
    //console.log('Service Worker � suportado.');
    navigator.serviceWorker.register('sw.js').then(function (reg) {
        //console.log('Service Worker registrado com sucesso.');
        reg.pushManager.subscribe({
            userVisibleOnly: true
        }).then(function (subscription) {
            // Enviar o objeto subscription para API e gravar os dados da assinatura
            // subscription.endpoint
            // subscription.keys.p256dh
            // subscription.keys.auth
            console.log(JSON.stringify(subscription));
        });
    }).catch(function (error) {
        console.log('Erro ao tentar registrar o Service Worker.', error);
    });
}