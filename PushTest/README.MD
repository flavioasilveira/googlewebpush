﻿# Google Push Notifiction With Payload Encripted

Code example using C#, HTML and JavaScript

## 1. Change data

Procurar e alterar no código pelos dados da sua aplicação do Google
```
<YOUR_API_KEY>
<YOUR_GMC_SENDER_ID>
```
## Breve descrição de cada passo

1. servico.html
	Solicita a permissao do usuario e registra a assinatura
2. servico.js
	Usuario aceita e Google retorna "subscription". Enviar o objeto subscription para uma API para persistir os dados da assinatura do usuario
3. sendpush.html
	Enviar os dados persistidos da assinatura do usuario a ser notificado e os dados da notificacao (texto, titulo, icone e url) para o WebAPI
4. SendPushController.cs
	Recebe o objeto do tipo "subscription" com o payload criptografado vindo pelo sendpush.html e envia para o servico do Google via HttpRequest

## Por fim, usuario recebe a notificacao personalizada.

## Credits
Thanks for the js lib by Matthew Gaunt - https://github.com/gauntface/simple-push-demo

## References
https://developers.google.com/web/updates/2016/03/web-push-encryption

### Qualquer dúvida, fico a disposição!