﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;

namespace PushTest.Controllers
{
    public class SendPushController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        public string Post(Notification notification)
        {
            string googleProjectId = "<YOUR_GMC_SENDER_ID>";

            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);

            byte[] byteArray = Encoding.UTF8.GetBytes(notification.body);

            HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(notification.endpoint);

            Request.Method = "POST";
            Request.KeepAlive = false;
            Request.ContentType = notification.headers.ContentType;

            Request.Headers.Add(string.Format("Authorization: {0}", notification.headers.Authorization));
            Request.Headers.Add(string.Format("Sender: id={0}", googleProjectId));
            Request.ContentLength = byteArray.Length;

            Request.Headers.Add(string.Format("Content-Encoding: {0}", notification.headers.ContentEncoding));
            Request.Headers.Add(string.Format("Encryption: {0}", notification.headers.Encryption));
            Request.Headers.Add(string.Format("Crypto-Key: {0}", notification.headers.CryptoKey));

            Stream dataStream = Request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            try
            {
                WebResponse Response = Request.GetResponse();
                HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    return "Sem autorização - token do google inválido.";
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    return "Sem resposta do endpoint.";
                }

                StreamReader Reader = new StreamReader(Response.GetResponseStream());
                string responseLine = Reader.ReadToEnd();
                Reader.Close();

                return responseLine;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        internal static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        [AllowAnonymous]
        public class Notification
        {
            public Headers headers { get; set; }
            public string body { get; set; }
            public string endpoint { get; set; }
        }

        [AllowAnonymous]
        public class Headers
        {
            public string Authorization { get; set; }
            public string ContentType { get; set; }
            public string Encryption { get; set; }
            public string CryptoKey { get; set; }
            public string ContentEncoding { get; set; }
        }
    }
}